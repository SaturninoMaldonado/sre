//////////////////////////////////////////////////
//////////////////////////////////////////////////
// Eliptic program for arduino nano
//
// - PWM output in pin D3
// - Analog read from potentiometer A2
// - Encoder reading A2
//////////////////////////////////////////////////
//////////////////////////////////////////////////
#define VERSION "V1.00nano"

#define DEBUG    1            //Enable debugging features WARNING: Might slow program

// Auxiliar variables to keep micros() at encoders
const int     PWM_PIN_R = 3;           // Define D3 Arduino Nano PWM Output
const int     PWM_PIN_L = 5;           // Define D5 Arduino Nano PWM Output


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
void setup() 
{
  
  pinMode(6, INPUT_PULLUP);           // set pin to input
  pinMode(7, INPUT_PULLUP);           // set pin to input
  pinMode(8, INPUT_PULLUP);           // set pin to input
  pinMode(9, INPUT_PULLUP);           // set pin to input

  //attachInterrupt(0, contador, FALLING);  // Define D2 as interrupt pin (interrupt 0)
  pinMode(PWM_PIN_L, OUTPUT);               // Define output pin for Motor PWM 
  pinMode(PWM_PIN_R, OUTPUT);               // Define output pin for Motor PWM 
  pinMode(A2,OUTPUT);
  digitalWrite(A2,LOW);
  pinMode(A3,OUTPUT);
  digitalWrite(A3,LOW);
  pinMode(A4,OUTPUT);
  digitalWrite(A4,LOW);
  pinMode(A5,OUTPUT);
  digitalWrite(A5,LOW);
  pinMode(A6,OUTPUT);
  digitalWrite(A6,LOW);
  
  digitalWrite(PWM_PIN_L, LOW);             // Start with PWM to 0
  digitalWrite(PWM_PIN_R, LOW);             // Start with PWM to 0
  Serial.begin(38400);                     // Initiating Serial communication

  #ifdef DEBUG
       Serial.println("");
       Serial.println("Iniciado");
  #endif

}  // End of setup()



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
void loop() 
{
  #define VEL_INICIAL     50
//  #define VEL_FINAL_RECTO 100  // 24V
  #define VEL_FINAL_RECTO 255  // 12V
  #define VEL_FINAL_CURVA 90   //24V 
  #define VEL_FINAL_CURVA 200  //12V 
  
  
  float SPEED_MOTOR_RELACTION=1.5;  // RIGHT MOTOR SPEED  / LEFT_MOTOR_SPEED
 
  
  float aceleracion=1.01;
  static float velocidad=VEL_INICIAL;
  static unsigned char estado=0;
  float vel_aux;
  
  if (digitalRead(6)==0)
  {
    // ADELANTE
    if (estado!=6)
      velocidad=VEL_INICIAL;
    estado=6;
    Serial.print(velocidad);
    Serial.print(" estado 6 ");
    
    // SIEMPRE PRIMERO LOW y DESPUES HIGH
    digitalWrite(A2,LOW);   // MOTOR IZQUIERDO
    digitalWrite(A4,LOW);   // MOTOR DERECHO
    digitalWrite(A3,HIGH);  // MOTOR IZQUIERDO
    digitalWrite(A5,HIGH);  // MOTOR DERECHO
    
    SPEED_MOTOR_RELACTION=(float)0.98;
    vel_aux=((float)velocidad*SPEED_MOTOR_RELACTION);
    analogWrite(PWM_PIN_R,(unsigned char) vel_aux);   // PWM  0-255
    analogWrite(PWM_PIN_L,(unsigned char) velocidad );   // PWM  0-255
    Serial.print(velocidad);
    Serial.print("  ");
    Serial.println(vel_aux);
    velocidad*=aceleracion;
    if (velocidad>VEL_FINAL_RECTO)
      velocidad=VEL_FINAL_RECTO;
  }
  else
    if (digitalRead(7)==0)
    {
      // ATRAS
      if (estado!=7)
        velocidad=VEL_INICIAL;
      estado=7;
    Serial.print(velocidad);
    Serial.println(" estado 7 ");

      // SIEMPRE PRIMERO LOW y DESPUES HIGH
      digitalWrite(A3,LOW);   // MOTOR IZQUIERDO
      digitalWrite(A5,LOW);   // MOTOR DERECHO
      digitalWrite(A2,HIGH);  // MOTOR IZQUIERDO
      digitalWrite(A4,HIGH);  // MOTOR DERECHO
 
      SPEED_MOTOR_RELACTION=(float)0.95;
      vel_aux=((float)velocidad*SPEED_MOTOR_RELACTION);
      analogWrite(PWM_PIN_L,(unsigned char) velocidad);   // PWM  0-255
      analogWrite(PWM_PIN_R,(unsigned char) vel_aux);   // PWM  0-255
      velocidad*=aceleracion;
      if (velocidad>VEL_FINAL_RECTO)
        velocidad=VEL_FINAL_RECTO;
     }
    else
      if (digitalRead(8)==0)
      {
        //IZQUIERDA
        if (estado!=8)
          velocidad=VEL_INICIAL;
        estado=8;
        Serial.print(velocidad);
        Serial.println(" estado 8 ");
        
        // SIEMPRE PRIMERO LOW y DESPUES HIGH
        digitalWrite(A3,LOW);   // MOTOR IZQUIERDO
        digitalWrite(A2,HIGH);  // MOTOR IZQUIERDO
        digitalWrite(A4,LOW);   // MOTOR DERECHO
        digitalWrite(A5,HIGH);  // MOTOR DERECHO
        
        SPEED_MOTOR_RELACTION=1.0;
//        vel_aux=((float)0.9*velocidad*SPEED_MOTOR_RELACTION); //0.9 gira mas rpido
        vel_aux=((float)velocidad*SPEED_MOTOR_RELACTION); //0.9 gira mas rpido

        analogWrite(PWM_PIN_L,vel_aux);   // PWM  0-255  
        analogWrite(PWM_PIN_R,(unsigned char) ((float)0.9*velocidad));   // PWM  0-255
        velocidad*=aceleracion;
        if (velocidad>VEL_FINAL_CURVA)
          velocidad=VEL_FINAL_CURVA;
      }
      else
        if (digitalRead(9)==0)
        {
          //DERECHA
          if (estado!=9)
            velocidad=VEL_INICIAL;
          estado=9;
          Serial.print(velocidad);
          Serial.println(" estado 9 ");
          
          // SIEMPRE PRIMERO LOW y DESPUES HIGH
          digitalWrite(A2,LOW);   // MOTOR IZQUIERDO
          digitalWrite(A3,HIGH);  // MOTOR IZQUIERDO
          digitalWrite(A5,LOW);   // MOTOR DERECHO
          digitalWrite(A4,HIGH);  // MOTOR DERECHO
          
          SPEED_MOTOR_RELACTION=1.0;
          vel_aux=((float)velocidad*SPEED_MOTOR_RELACTION); 
          analogWrite(PWM_PIN_L,vel_aux);   // PWM  0-255
          analogWrite(PWM_PIN_R,(unsigned char) velocidad);   // PWM  0-255
          velocidad*=aceleracion;
          if (velocidad>VEL_FINAL_CURVA)
            velocidad=VEL_FINAL_CURVA;
        }
        else
        {
          estado=0;
          velocidad=0;

          Serial.print(velocidad);
          Serial.println(" estado 0 (reposo) ");

          // SIEMPRE PRIMERO LOW y DESPUES HIGH
          digitalWrite(A3,LOW);   // MOTOR IZQUIERDO
          digitalWrite(A5,LOW);  // MOTOR IZQUIERDO
          digitalWrite(A2,LOW);   // MOTOR DERECHO
          digitalWrite(A4,LOW);  // MOTOR DERECHO
          analogWrite(PWM_PIN_L,(unsigned char) velocidad);   // PWM  0-255
          analogWrite(PWM_PIN_R,(unsigned char) velocidad);   // PWM  0-255
        }  
}//end loop()

