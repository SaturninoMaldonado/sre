 #pragma GCC optimize ("-O2")
#include <SoftwareSerial.h>                                                                                                //Libreria para controlar el BlueTooth.
#include <VarSpeedServo.h>                                                                                                 //Libreria para la velocidad de los servos.

SoftwareSerial BT1(1,0);                                                                                                   //Seleccionamos RX y TX para BlueTooth.
VarSpeedServo servo1; VarSpeedServo servo2;                                                                                //Creamos los objetos para ambos servos.

#define encDER 3
#define encIZQ 2
#define intRele 4
#define ser1 9
#define ser2 5
#define avanzaIZQ 7
#define atrasDER 10
#define avanzaDER 8
#define atrasIZQ 12
#define pwmIZQ 11
#define pwmDER 6
#define finDER A1
#define cnyDER A0
#define finCEN A5
#define finIZQ A4
#define cnyIZQ A3
#define cnyCEN A2
#define BUZZER 13
#define LED 13
#define pulsosgradosgrande 16

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~VARIABLES GLOBALES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
static uint8_t velIZQ, velDER;
uint8_t i = 0;
char inChar;
char orden[1];
unsigned int encoderIZQ = 0, encoderDER = 0, encoderLIM = 0;
uint16_t valor = 0;
char auxencoder = '0';
char auxbloqueo = '0';
char auxatrasizq = '0';
char auxatrasder = '0';
uint8_t velservo = 30;
uint8_t poservo = 0;
unsigned int limitencoder = 0;
uint16_t grados = 0;
uint8_t enc = 0;
float pulsosgrados = 9.5;
char sensores[7];
char encoderizq[20];
char encoderder[7];


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DECL. FUNCIONES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void analizar_orden(void);
short int leer_numero(void);
void cuentaDER()
{
  encoderDER++;                                                                                                              //Cada vez que detecta un pulso sube una cuenta.
}

void cuentaIZQ()
{
  encoderIZQ++;                                                                                                            //Cada vez que detecta un pulso sube una cuenta.
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~SETUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void setup()
{
  pinMode(pwmDER, OUTPUT);                                                                                                   //Declaramos los pines correspondientes como entradas o salidas.
  pinMode(pwmIZQ, OUTPUT);
  pinMode(ser1, OUTPUT);
  pinMode(ser2, OUTPUT);
  pinMode(intRele, OUTPUT);
  pinMode(avanzaDER, OUTPUT);
  pinMode(atrasDER, OUTPUT);
  pinMode(avanzaIZQ, OUTPUT);
  pinMode(atrasIZQ, OUTPUT);
  pinMode(BUZZER, OUTPUT);

  pinMode(cnyIZQ, INPUT);
  pinMode(cnyDER, INPUT);
  pinMode(cnyCEN, INPUT);
  pinMode(finIZQ, INPUT);
  pinMode(finDER, INPUT);
  pinMode(finCEN, INPUT);
  pinMode(encDER, INPUT);
  pinMode(encIZQ, INPUT);

  attachInterrupt(digitalPinToInterrupt(3), cuentaDER, FALLING);                                                             //Configura las int. ext. por flanco de bajada.
  attachInterrupt(digitalPinToInterrupt(2), cuentaIZQ, FALLING);                                                             //Asigna la funcion correspondiente para cada int.


  digitalWrite(intRele, LOW);
  servo1.attach(ser1);                                                                                                       //Analog pin 5.
  servo2.attach(ser2);                                                                                                       //Analog pin 6.

  servo1.slowmove(65, 100);
  servo2.slowmove(65, 100);
  digitalWrite(intRele, HIGH);
  delay(2500);

  servo1.detach();
  servo2.detach();

  digitalWrite(intRele, LOW);


  analogWrite(pwmDER, 0);                                                                                                    //Colocamos los motores a 0 por precaucion.
  analogWrite(pwmIZQ, 0);

  digitalWrite(avanzaDER, LOW);
  digitalWrite(avanzaIZQ, LOW);
  digitalWrite(atrasDER, LOW);
  digitalWrite(atrasIZQ, LOW);

  Serial.begin(9600);                                                                                                        //Establecemos la conexion con el puerto serie o BlueTooth.

  Serial.println("D");
  digitalWrite(13, HIGH);
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MAIN~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void loop()
{
  digitalWrite(LED, LOW);
  if (Serial.available() > 0)                                                                                                //Si hay algo disponible en el puerto serie lo lee y lo
  { 
    Serial.readBytes(orden, 1);
    analizar_orden();                                                                                                       //Llama a la funcion "analizar_cadena".
  }

  if(auxencoder == '1')
  {
    if(encoderDER>=limitencoder)
    {
      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, HIGH);
      analogWrite(pwmDER, 255);
      analogWrite(pwmIZQ, 255);
      limitencoder = 0;
      encoderDER = 0;
      encoderIZQ = 0;
      auxencoder = 0;
    }
    if(encoderIZQ>=limitencoder)
    {
      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, HIGH);
      analogWrite(pwmDER, 255);
      analogWrite(pwmIZQ, 255);
      limitencoder = 0;
      encoderDER = 0;
      encoderIZQ = 0;
      auxencoder = 0;
    }
  }
  /*if(digitalRead(cnyIZQ)==HIGH||digitalRead(cnyDER)==HIGH||digitalRead(cnyCEN)==HIGH||digitalRead(finIZQ)==LOW||digitalRead(finDER)==LOW||digitalRead(finCEN)==LOW)
   {
      if(auxbloqueo == '0')
      {
      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, HIGH);
      analogWrite(pwmDER, 255);
      analogWrite(pwmIZQ, 255);
      }
   }*/
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ANALIZAR CADENA~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void analizar_orden()
{
  uint8_t AUXsensor;

  switch (orden[0])
  {
    case 48:                                                                                                               //ASCII 48 -> AVANZA A VELOCIDAD X.
      valor = leer_numero();
      auxencoder = '0';
      auxbloqueo = '0';

      velDER = valor;
      velIZQ = valor;
      encoderDER = 0;
      encoderIZQ = 0;
      
      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, LOW);
      digitalWrite(atrasIZQ, LOW);

      analogWrite(pwmDER, velDER);                                                                                         //Actualiza las pwm para sus respectivas velocidades.
      analogWrite(pwmIZQ, velIZQ);

      break;

    case 49:                                                                                                               //ASCII 49 -> AVANZA A VELOCIDADES DISTINTAS.
      valor = leer_numero();
      velIZQ = valor;
      valor = leer_numero();
      velDER = valor;

      auxencoder = '0';
      auxbloqueo = '0';

      encoderDER = 0;
      encoderIZQ = 0;
      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, LOW);
      digitalWrite(atrasIZQ, LOW);

      analogWrite(pwmDER, velDER);                                                                                         //Actualiza las pwm para sus respectivas velocidades.
      analogWrite(pwmIZQ, velIZQ);

      break;

    case 50:                                                                                                               //ASCII 50 -> RETROCEDE A VELOCIDAD X.
      valor = leer_numero();
      auxencoder = '0';
      auxbloqueo = '1';
      Serial.println("atras");
      velDER = valor;
      velIZQ = valor;

      encoderDER = 0;
      encoderIZQ = 0;
      digitalWrite(avanzaDER, LOW);
      digitalWrite(avanzaIZQ, LOW);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, HIGH);

      analogWrite(pwmDER, velDER);                                                                                         //Actualiza las pwm para sus respectivas velocidades.
      analogWrite(pwmIZQ, velIZQ);

      break;

    case 51:                                                                                                               //ASCII 51 -> RETROCEDE A VELOCIDADADES DISTINTAS.
      valor = leer_numero();
      velIZQ = valor;
      auxencoder = '0';
      auxbloqueo = '1';
      valor = leer_numero();
      velDER = valor;

      encoderDER = 0;
      encoderIZQ = 0;
      digitalWrite(avanzaDER, LOW);
      digitalWrite(avanzaIZQ, LOW);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, HIGH);

      analogWrite(pwmDER, velDER);                                                                                         //Actualiza las pwm para sus respectivas velocidades.
      analogWrite(pwmIZQ, velIZQ);

      break;

    case 52:                                                                                                               //ASCII 52 -> GIRA IZQUIERDA X GRADOS A VELOCIDAD DEFINIDA.
      auxencoder = '1';
      auxbloqueo = '0';
      valor = leer_numero();
      grados = valor;
      if(grados>360)
        grados = 360;
      if(grados<0)
        grados = 0;
      valor = leer_numero();
      velDER = valor;
      velIZQ = valor;
      limitencoder = grados * pulsosgrados;
      encoderDER = 0;
      encoderIZQ = 0;

      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, LOW);
      digitalWrite(atrasDER, LOW);
      digitalWrite(atrasIZQ, HIGH);

      analogWrite(pwmDER, velDER);
      analogWrite(pwmIZQ, velIZQ);

      break;

    case 53:                                                                                                               //ASCII 53 -> GIRA DERECHA X GRADOS A VELOCIDAD DEFINIDA.
      auxencoder = '1';
      auxbloqueo = '0';
      valor = leer_numero();
      grados = valor;
      if(grados>360)
        grados = 360;
      if(grados<0)
        grados = 0;
      valor = leer_numero();
      velDER = valor;
      velIZQ = valor;
      limitencoder = grados * pulsosgrados;
      encoderDER = 0;
      encoderIZQ = 0;

      digitalWrite(avanzaDER, LOW);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, LOW);

      analogWrite(pwmDER, velDER);
      analogWrite(pwmIZQ, velIZQ);

      break;

    case 54:                                                                                                               //ASCII 54 -> GIRA RESPECTO RUEDA DERECHA ADELANTE X GRADOS A VELOCIDAD DEFINIDA.
      auxencoder = '1';
      auxbloqueo = '0';
      valor = leer_numero();
      grados = valor;
      if(grados>360)
        grados = 360;
      if(grados<0)
        grados = 0;
      valor = leer_numero();
      velIZQ = valor;
      limitencoder = grados * pulsosgradosgrande;
      encoderDER = 0;
      encoderIZQ = 0;

      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, LOW);

      analogWrite(pwmDER, 255);
      analogWrite(pwmIZQ, velIZQ);

      break;

    case 55:                                                                                                               //ASCII 55 -> GIRA RESPECTO RUEDA DERECHA ATRAS X GRADOS A VELOCIDAD DEFINIDA.
      auxencoder = '1';
      auxbloqueo = '0';
      valor = leer_numero();
      grados = valor;
      if(grados>360)
        grados = 360;
      if(grados<0)
        grados = 0;
      valor = leer_numero();
      velIZQ = valor;
      limitencoder = grados * pulsosgradosgrande;
      encoderDER = 0;
      encoderIZQ = 0;

      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, LOW);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, HIGH);

      analogWrite(pwmDER, 255);
      analogWrite(pwmIZQ, velIZQ);

      break;

    case 56:                                                                                                               //ASCII 56 -> GIRA RESPECTO RUEDA IZQUIERDA ADELANTE X GRADOS A VELOCIDAD DEFINIDA.
      auxencoder = '1';
      auxbloqueo = '0';
      valor = leer_numero();
      grados = valor;
      if(grados>360)
        grados = 360;
      if(grados<0)
        grados = 0;
      valor = leer_numero();
      velDER = valor;
      limitencoder = grados * pulsosgradosgrande;
      encoderDER = 0;
      encoderIZQ = 0;

      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, LOW);
      digitalWrite(atrasIZQ, HIGH);

      analogWrite(pwmDER, velDER);
      analogWrite(pwmIZQ, 255);

      break;

    case 57:                                                                                                               //ASCII 57 -> GIRA RESPECTO RUEDA IZQUIERDA ATRAS X GRADOS A VELOCIDAD DEFINIDA.
      auxencoder = '1';
      auxbloqueo = '0';
      valor = leer_numero();
      grados = valor;
      if(grados>360)
        grados = 360;
      if(grados<0)
        grados = 0;
      valor = leer_numero();
      velDER = valor;
      limitencoder = grados * pulsosgradosgrande;
      encoderDER = 0;
      encoderIZQ = 0;

      digitalWrite(avanzaDER, LOW);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, HIGH);

      analogWrite(pwmDER, velDER);
      analogWrite(pwmIZQ, 255);

      break;
      
    
    case 58:                                                                                                               //ASCII 58 -> ESTADO DE LOS SENSORES.
      sensores[0] = '(';
      if(digitalRead(cnyIZQ)==HIGH)
         sensores[1]='1';
      else
         sensores[1]='0';
      if(digitalRead(cnyDER)==HIGH)
         sensores[2]='1';
      else
         sensores[2]='0';
      if(digitalRead(cnyCEN)==HIGH)
         sensores[3]='1';
      else
         sensores[3]='0';
      if(digitalRead(finIZQ)==HIGH)
         sensores[4]='1';
      else
         sensores[4]='0';
      if(digitalRead(finDER)==HIGH)
         sensores[5]='1';
      else
         sensores[5]='0';
      if(digitalRead(finCEN)==HIGH)
         sensores[6]='1';
      else
         sensores[6]='0';        
      Serial.println(sensores);

      break;

    case 59:                                                                                                               //ASCII 59 -> ESTADO DE LOS ENCODERS.
      itoa(encoderIZQ,encoderizq, 7);
      itoa(encoderDER,encoderder, 7);
      encoderizq[6]=encoderizq[5];
      encoderizq[5]=encoderizq[4];
      encoderizq[4]=encoderizq[3];
      encoderizq[3]=encoderizq[2];
      encoderizq[2]=encoderizq[1];
      encoderizq[1]=encoderizq[0];
      encoderizq[0]=')';     
      encoderder[6]=encoderder[5];
      encoderder[5]=encoderder[4];
      encoderder[4]=encoderder[3];
      encoderder[3]=encoderder[2];
      encoderder[2]=encoderder[1];
      encoderder[1]=encoderder[0];
      encoderder[0]='*'; 
      strcat(encoderizq, encoderder);
      Serial.println(encoderizq);

      break;

    case 60:                                                                                                               //ASCII 60 -> RESET DE LA CUENTA DE AMBOS ENCODERS.
      encoderDER = 0;
      encoderIZQ = 0;
      Serial.println("'");

      break;

    case 61:                                                                                                               //ASCII 61 -> POSICIONAR SERVO 1.
      valor = leer_numero();
      poservo = valor;

      servo1.attach(ser1);                                                                                                  
      servo2.attach(ser2); 
      digitalWrite(intRele, HIGH);
      servo1.slowmove(poservo, velservo);
      delay(2500);
      servo1.detach();
      servo2.detach();
      digitalWrite(intRele, LOW);
      Serial.println("+");
      
      break;

    case 62:                                                                                                               //ASCII 62 -> POSICIONAR SERVO 2.
      valor = leer_numero();
      poservo = valor;

      servo2.attach(ser2);                                                                                                     
      servo1.attach(ser1); 
      digitalWrite(intRele, HIGH);
      servo2.slowmove(poservo, velservo);
      delay(2500);
      servo2.detach();
      servo1.detach();
      digitalWrite(intRele, LOW);
      Serial.println("+");
      
      break;

    case 63:                                                                                                               //ASCII 63 -> DETENER AMBOS MOTORES.
      auxencoder = 0;
      velDER = 0;
      velIZQ = 0;

      digitalWrite(avanzaDER, HIGH);
      digitalWrite(avanzaIZQ, HIGH);
      digitalWrite(atrasDER, HIGH);
      digitalWrite(atrasIZQ, HIGH);

      analogWrite(pwmDER, 255);                                                                                            //Actualiza las pwm para sus respectivas velocidades.
      analogWrite(pwmIZQ, 255);

      break;

    case 64:                                                                                                              //ASCII 64 -> Recoger
    
      servo2.attach(ser2);                                                                                                     
      servo1.attach(ser1); 
      
      digitalWrite(intRele, HIGH);
      servo1.slowmove(61, 25);
      servo2.slowmove(140, 25);
      delay(2500);
      servo1.slowmove(43, 25);
      servo2.slowmove(140, 25);
      delay(400);
      servo1.slowmove(43, 25);
      servo2.slowmove(115, 25);
      delay(400);
      servo1.slowmove(48, 25);
      servo2.slowmove(67, 25);
      delay(900);
      servo2.detach();
      servo1.detach();
      digitalWrite(intRele, LOW);
      Serial.println(",");
      digitalWrite(13, HIGH);
      
      break;
      
    case 65:                                                                                                              //ASCII 65 -> Presentar
    
      servo2.attach(ser2);                                                                                                     
      servo1.attach(ser1); 
      
      digitalWrite(intRele, HIGH);
      servo1.slowmove(0, 20);
      servo2.slowmove(67, 20);
      delay(2200);
      servo2.detach();
      servo1.detach();
      digitalWrite(intRele, LOW);
      Serial.println("-");
      digitalWrite(13, HIGH);
      
      break;

    case 66:                                                                                                              //ASCII 66 -> Volver
          
      servo2.attach(ser2);                                                                                                     
      servo1.attach(ser1); 
      
      digitalWrite(intRele, HIGH);
      servo1.slowmove(53, 15);
      servo2.slowmove(67, 20);
      delay(1300);
      servo1.slowmove(61, 25);
      servo2.slowmove(140, 25);
      delay(1500);
      servo2.detach();
      servo1.detach();
      digitalWrite(intRele, LOW);
      Serial.println(".");
      digitalWrite(13, HIGH);
      
      break;

    case 67:                                                                                                            //ASCII 67 -> Revolver

      servo2.attach(ser2);                                                                                                     
      servo1.attach(ser1); 
      
      digitalWrite(intRele, HIGH);
      servo1.slowmove(61, 25);
      servo2.slowmove(61, 25);
      delay(1000);
      servo1.slowmove(48, 25);
      servo2.slowmove(61, 25);
      delay(700);
      servo1.slowmove(43, 25);
      servo2.slowmove(110, 25);
      delay(800);
      servo1.slowmove(48, 25);
      servo2.slowmove(130, 25);
      delay(400);
      servo1.slowmove(61, 20);
      servo2.slowmove(135, 20);
      delay(1000);
      servo2.detach();
      servo1.detach();
      digitalWrite(intRele, LOW);
      Serial.println("/");
      digitalWrite(13, HIGH);
      break;

    case 68:
      digitalWrite(BUZZER, HIGH);
      delay(1000);
      digitalWrite(BUZZER, LOW);
      break;

    case 69:
      valor = leer_numero();
      if(valor>127)
      {
        valor=valor-127;
        auxatrasizq='1';
      }
      else if(valor<=127)
      {
        auxatrasizq='0';
      }
      velIZQ = valor*2;
      Serial.println(velIZQ);
      valor = leer_numero();
      if(valor>127)
      {
        valor=valor-127;
        auxatrasder='1';
      }
      else if(valor<=127)
      {
        auxatrasder='0';
      }
      velDER = valor*2;
      Serial.println(velDER);

      analogWrite(pwmDER, velDER);                                                                                         //Actualiza las pwm para sus respectivas velocidades.
      analogWrite(pwmIZQ, velIZQ);

      if(auxatrasder=='0')
      {
        digitalWrite(avanzaDER, HIGH);
        digitalWrite(atrasDER, LOW);
        Serial.println("avanzaDER");
      }
      else if(auxatrasder=='1')
      {
        digitalWrite(avanzaDER, LOW);
        digitalWrite(atrasDER, HIGH);
        Serial.println("atrasDER");
      }
      if(auxatrasizq=='0')
      {
        digitalWrite(avanzaIZQ, HIGH);
        digitalWrite(atrasIZQ, LOW);
      }
      else if(auxatrasizq=='1')
      {
        digitalWrite(avanzaIZQ, LOW);
        digitalWrite(atrasIZQ, HIGH);
      }

      break;
    
  }
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~LEER NUMERO~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
short int leer_numero()
{
  uint16_t val = 0;
  char velocidad[3];
  Serial.readBytes(velocidad, 3);
  val = atoi(velocidad);
  return val;                                                                                                             //Devuelve la variable VAL
}
