
/***********************************************************************************
                              Joytick Tester Silla UBC
************************************************************************************/
/* Este programa se ha desarrollado para controlar la silla de ruedas UBC
    está pensado para dirigir la silla con un Joystick de dos ejes perpendiculares,
    los cuales nos permitencontrolar los motores, dirección, sentido y velocidad.
    Tiene además un indicador de nivel de bateria, indicador de nivel de velocidad
    en formato de array LED de 10 segmentos y aviso acustico mediante un pulsador.
    Posee como entradas dos pulsadores para aumenar y reducir la velocidad.
    El cual leemos mediante un pin analogico del arduino.
    El arduino genera las señales de control de los motores, PWM, DIRECCION, ENABLE.
*/

/*********************************************************************************************

                  +VY
                  1023
                   ^
                   |
      -VX    <----POT---->     +VX
                   |
                   ^
                   0
                  -VY

**********************************************************************************************/


//Definimos el numero del pin por el que van a salir o entrar los datos del sistema.
//#define Referencia_bat       //Señal de entrada del nivel de bateria, tipo analogica.
#define Referencia_vel_in   A0   //Señal de entrada para modificar la velocidad, tipo analogica.
#define Referencia_vel_out  10   //Señal de salida para indicar la velocidad en los leds, tipo analogica.
#define Referencia_bat_in   A1   //Señal de entrada para modificar la velocidad, tipo analogica.
#define Referencia_bat_out  9   //Señal de salida para indicar la velocidad en los leds, tipo analogica.
#define Dir_Derecha1        2    //Señal de salida DIRECCION para motor derecha, tipo booleana.
#define Dir_Derecha2        11    //Señal de salida DIRECCION para motor derecha, tipo booleana.
#define PWM_Derecha         3    //Señal de salida PWM para motor derecha, tipo digital.
#define Enable_Derecha      4    //Señal de salida ENABLE para el rele del motor derecha, tipo booleana.
#define Dir_Izquierda1      5    //Señal de salida DIRECCION para motor izquierda, tipo booleana.
#define Dir_Izquierda2      12    //Señal de salida DIRECCION para motor izquierda, tipo booleana.
#define PWM_Izquierda       6    //Señal de salida PWM para motor izquierda, tipo digital.
#define Enable_Izquierda    7    //Señal de salida ENABLE para el rele del motor izquierda, tipo booleana.
#define Pin_Vx              A2   //Señal de entrada para controlar motor, tipo analogica.
#define Pin_Vy              A3   //Señal de entrada para controlar motor, tipo analogica.


#define VEL  254
#define rad2grad 180/PI

#define VELOCIDAD_FW          0.9
#define VELOCIDAD_RW          0.4
#define VELOCIDAD_GIRO        0.7
#define VELOCIDAD_SOLO_GIRO   0.4

float Factor_vel = 0.5;

#include "Math.h"

int lim_inf = 450;      //limites para el cuadrado de reposo.
int lim_sup = 600;

int vx; //Valor medido del eje x del joystick
int vy; //Valor medido del eje y del joystick
int accion_vel;
int nivel_velocidad = 0;
int aceleracion;
int velocidad_disp = 0;
int  nivel_bat = 0;

int sentido_adelante = 0, sentido_derecha = 0, sentido_izquierda = 0, sentido_atras = 0;

double angulo = 0;


void setup() // put your setup code here, to run once:
{
  Serial.begin (9600); // Se configura la velocidad de comunicacion en 9600 baudios.

  pinMode (Referencia_vel_in,   INPUT);
  pinMode (Referencia_vel_out,  OUTPUT);
  pinMode (Referencia_bat_in,   INPUT);
  pinMode (Referencia_bat_out,  OUTPUT);
  pinMode (Dir_Derecha1,         OUTPUT);
  pinMode (Dir_Derecha2,         OUTPUT);
  pinMode (PWM_Derecha,         OUTPUT);
  pinMode (Enable_Derecha,      OUTPUT);            //Cable MARRON de los reles
  pinMode (Dir_Izquierda1,       OUTPUT);
  pinMode (Dir_Izquierda2,       OUTPUT);
  pinMode (PWM_Izquierda,       OUTPUT);
  pinMode (Enable_Izquierda,    OUTPUT);            //Cable AZUL de los reles
  pinMode (Pin_Vx,              INPUT);
  pinMode (Pin_Vy,              INPUT);
}

/************  FUNCION PARA EL CALCULO DEL ANGULO DE JOYSTICK.  ****************/
void medir_angulo ()
{
  double vx_atan2 = vx - 511;
  double vy_atan2 = vy - 511;

  angulo = atan2(vy_atan2, vx_atan2) * rad2grad;
  if (angulo < 0)
    angulo = 360 + angulo;
//Serial.print ("\n\n ANGULO = "); Serial.println (angulo); //imprimo el valor del ANGULO
  delay (50);
}

/************  FUNCION MEDIR NIVEL BATERIA  ****************/
void medir_bateria()
{
  int nivel =  analogRead(Referencia_bat_in);              //Valor medido para 3.85V => 860
  nivel_bat = map(nivel, 740, 850, 20, 210);        //Valor medido para 3.57V => 796
  if (nivel_bat <= 20)
  {nivel_bat = 20;}
  if ((nivel_bat > 20) && (nivel_bat <= 40))
  {nivel_bat = 30;}
  if ((nivel_bat > 40) && (nivel_bat <= 60))
  {nivel_bat = 50;}
  if ((nivel_bat > 60) && (nivel_bat <= 80))
  {nivel_bat = 70;}
  if ((nivel_bat > 80) && (nivel_bat <= 100))
  {nivel_bat = 90;}
  if ((nivel_bat > 100) && (nivel_bat <= 120))
  {nivel_bat = 110;}
  if ((nivel_bat > 120) && (nivel_bat <= 140))
  {nivel_bat = 130;}
  if ((nivel_bat > 140) && (nivel_bat <= 160))
  {nivel_bat = 150;}
  if ((nivel_bat > 160) && (nivel_bat <= 180))
  {nivel_bat = 170;}
  if ((nivel_bat > 180) && (nivel_bat <= 210))
  {nivel_bat = 200;}
  if (nivel_bat >= 210)
  {nivel_bat = 210;}

  analogWrite(Referencia_bat_out, nivel_bat);
  //Serial.print ("\n\n BATERIA LEIDA= "); Serial.println (nivel); //imprimo el valor del nivel bateria.
  //Serial.print ("\n\n BATERIA ESCRITA= "); Serial.println (nivel_bat); //imprimo el valor del nivel bateria.
}

/************  FUNCION MAIN  ****************/
void loop()
{
  //Posiblemente lo mejor será hacer un switch case
  float  a1, a2, a3, a4;
  float    velocidad;
  int vel;
  //los valores analogicos medidos varian entre 0 y 1023.
  vx = analogRead (Pin_Vx);    //Tomo el valor analogico del joystick para el eje x
  vy = analogRead (Pin_Vy);    //Tomo el valor analogico del joystick para el eje y
  accion_vel = analogRead (Referencia_vel_in);   //Tomo el valor codificado de los pulsadores +/-

  /*
    Serial.print ("\n\n Valor medido eje x = "); Serial.println (vx); //imprimo por el puerto serie el valor medido en Vx
    //  delay (50);
    Serial.print ("\n\n Valor medido eje y = "); Serial.println (vy); //imprimo por el puerto serie el valor medido en Vx
    //  delay (50);
    Serial.print ("\n\n Valor medido en el pulsador = "); Serial.println (accion_vel); //imprimo por el puerto serie el valor medido en Vx
    delay (50);
  */
  //Serial.println (vy); //imprimo por el puerto serie el valor medido en Vy
  //Serial.println (vx);

  medir_angulo();
  medir_bateria();

  //Compruebo el valor medido del potenciometro para mover los motores


  /******* CODIGO DE ANGULOS ************/

  if (((vx >= lim_inf) && (vx <= lim_sup)) && ((vy >= lim_inf) && (vy <= lim_sup))) //BLOQUEADOS
  {
    sentido_adelante = 0; sentido_derecha = 0; sentido_izquierda = 0; sentido_atras = 0;
    analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
    analogWrite (PWM_Izquierda, 0);
    digitalWrite (Enable_Derecha, false);      //Desconecta los dos reles.
    digitalWrite (Enable_Izquierda, false);
    //Serial.print ("\n\n Motor BLOQUEADO. ");   //Imprimo por el puerto serie para saber en que estado estoy.
    //   delay(50);
  }

  else if ((angulo < 120) && (angulo > 60)) //ADELANTE
  {
    if (sentido_adelante == 0)
    {
      sentido_adelante = 1;
      analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
      analogWrite (PWM_Izquierda, 0);
      sentido_derecha = 0; sentido_izquierda = 0; sentido_atras = 0;
    }
    digitalWrite (Dir_Derecha1, HIGH);          //Pongo las direcciones a 10
    digitalWrite (Dir_Derecha2, LOW);
    digitalWrite (Dir_Izquierda1, HIGH);         //Pongo las direcciones a 10
    digitalWrite (Dir_Izquierda2, LOW);
    digitalWrite (Enable_Derecha, true);       //Conecta los dos reles.
    digitalWrite (Enable_Izquierda, true);
    a1 = (vy - lim_sup);
    a2 = (1023 - lim_sup);
    velocidad = a1 * VEL / a2;
    vel = velocidad * VELOCIDAD_FW;
    float   vel_fw = vel * VELOCIDAD_FW * Factor_vel;
    analogWrite (PWM_Derecha, vel_fw);              //Habilito las dos PWM's
    analogWrite (PWM_Izquierda, vel_fw);
    //Serial.print ("\n\n Motor ADELANTE. ");    //Imprimo por el puerto serie para saber en que estado estoy.
    delay(250);
  }

  else if ((angulo < 20) || (angulo > 340)) //DERECHA
  {
    if (sentido_derecha == 0)
    {
      sentido_derecha = 1;
      analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
      analogWrite (PWM_Izquierda, 0);
      sentido_adelante = 0; sentido_izquierda = 0; sentido_atras = 0;
    }
    digitalWrite (Dir_Derecha1, LOW);          //Pongo las direcciones a 01 ATRAS
    digitalWrite (Dir_Derecha2, HIGH);
    digitalWrite (Dir_Izquierda1, HIGH);       //Pongo las direcciones a 10 ADELANTE
    digitalWrite (Dir_Izquierda2, LOW);
    digitalWrite (Enable_Derecha, true);       //Conecta los dos reles.
    digitalWrite (Enable_Izquierda, true);
    a1 = (vx - lim_sup);
    a2 = (1023 - lim_sup);
    velocidad = a1 * VEL / a2;
    vel = velocidad;
    float     vel_der = vel * VELOCIDAD_SOLO_GIRO * Factor_vel;      //Correccion de la velocidad maxima mediante constantes de valor maximo 1
    analogWrite (PWM_Derecha, vel_der);          //Habilito las dos PWM's
    analogWrite (PWM_Izquierda, vel_der);
    //Serial.print ("\n\n Motor DERECHA. ");      //Imprimo por el puerto serie para saber en que estado estoy.
    delay(50);
  }

  else if ((angulo < 200) && (angulo > 160)) //IZQUIERDA
  {
    if (sentido_izquierda == 0)
    {
      sentido_izquierda = 1;
      analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
      analogWrite (PWM_Izquierda, 0);
      sentido_adelante = 0; sentido_derecha = 0; sentido_atras = 0;
    }
    analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
    analogWrite (PWM_Izquierda, 0);
    digitalWrite (Dir_Derecha1, HIGH);          //Pongo las direcciones a 10 ADELANTE
    digitalWrite (Dir_Derecha2, LOW);
    digitalWrite (Dir_Izquierda1, LOW);       //Pongo las direcciones a 01 ATRAS
    digitalWrite (Dir_Izquierda2, HIGH);
    digitalWrite (Enable_Derecha, true);       //Conecta los dos reles.
    digitalWrite (Enable_Izquierda, true);
    a1 = (lim_inf - vx);
    a2 = (lim_inf);
    velocidad = a1 * VEL / a2;
    vel = velocidad;
    float   vel_izq = vel * VELOCIDAD_SOLO_GIRO * Factor_vel;     //Correccion de la velocidad maxima mediante constantes de valor maximo 1
    analogWrite (PWM_Derecha, vel_izq);              //Habilito las dos PWM's
    analogWrite (PWM_Izquierda, vel_izq);
    //Serial.print ("\n\n Motor IZQUIERDA. ");   //Imprimo por el puerto serie para saber en que estado estoy.
    delay(50);
  }

  else if ((angulo < 300) && (angulo > 240)) //ATRAS
  {
    if (sentido_atras == 0)
    {
      sentido_atras = 1;
      analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
      analogWrite (PWM_Izquierda, 0);
      sentido_adelante = 0; sentido_derecha = 0; sentido_izquierda = 0;
    }
    analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
    analogWrite (PWM_Izquierda, 0);
    digitalWrite (Dir_Derecha1, LOW);          //Pongo las direcciones a 01 ATRAS.
    digitalWrite (Dir_Derecha2, HIGH);
    digitalWrite (Dir_Izquierda1, LOW);        //Pongo las direcciones a 01 ATRAS.
    digitalWrite (Dir_Izquierda2, HIGH);
    digitalWrite (Enable_Derecha, true);       //Conecta los dos reles.
    digitalWrite (Enable_Izquierda, true);
    a1 = (lim_inf - vy);
    a2 = (lim_inf);
    velocidad = a1 * VEL / a2;
    vel = velocidad;
    float vel_rw = vel * VELOCIDAD_RW * Factor_vel;                //Correccion de la velocidad maxima mediante constantes de valor maximo 1
    //Aqui se debe incluir la correccion por la posicion del joystick
    analogWrite (PWM_Derecha, vel_rw);              //Habilito las dos PWM's
    analogWrite (PWM_Izquierda, vel_rw);
    //Serial.print ("\n\n Motor ATRAS. ");       //Imprimo por el puerto serie para saber en que estado estoy.
    delay(50);
  }



  else if ((angulo > 20) && (angulo < 60)) //ADELANTE-DERECHA
  {
    if (sentido_derecha == 0)
    {
      sentido_derecha = 1;
      analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
      analogWrite (PWM_Izquierda, 0);
      sentido_adelante = 0; sentido_izquierda = 0; sentido_atras = 0;
    }
    digitalWrite (Dir_Derecha1, HIGH);         //Pongo las direcciones a 10 ADELANTE
    digitalWrite (Dir_Derecha2, LOW);
    digitalWrite (Dir_Izquierda1, HIGH);       //Pongo las direcciones a 10 ADELANTE
    digitalWrite (Dir_Izquierda2, LOW);

    digitalWrite (Enable_Derecha, true);       //Conecta los dos reles.
    digitalWrite (Enable_Izquierda, true);
    a1 = (vx - lim_sup);
    a2 = (1023 - lim_sup);
    velocidad = a1 * VEL / a2;
    vel = velocidad;
    int     vel_der = vel * VELOCIDAD_GIRO * Factor_vel;      //Correccion de la velocidad maxima mediante constantes de valor maximo 1
    float der_ade = map(angulo, 20, 60, 0, 100);
    der_ade = der_ade / 100;
    analogWrite (PWM_Derecha, vel_der);        //Habilito las dos PWM's
    analogWrite (PWM_Izquierda, (vel_der) * der_ade);
    //Serial.print ("\n\n Motor DERECHA. ");      //Imprimo por el puerto serie para saber en que estado estoy.
    delay(50);
  }

  else if ((angulo < 160) && (angulo > 120)) //ADELANTE-IZQUIERDA
  {
    if (sentido_izquierda == 0)
    {
      sentido_izquierda = 1;
      analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
      analogWrite (PWM_Izquierda, 0);
      sentido_adelante = 0; sentido_derecha = 0; sentido_atras = 0;
    }
    analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
    analogWrite (PWM_Izquierda, 0);

    digitalWrite (Dir_Derecha1, HIGH);         //Pongo las direcciones a 10 ADELANTE
    digitalWrite (Dir_Derecha2, LOW);
    digitalWrite (Dir_Izquierda1, HIGH);       //Pongo las direcciones a 10 ADELANTE
    digitalWrite (Dir_Izquierda2, LOW);

    digitalWrite (Enable_Derecha, true);       //Conecta los dos reles.
    digitalWrite (Enable_Izquierda, true);
    a1 = (lim_inf - vx);
    a2 = (lim_inf);
    velocidad = a1 * VEL / a2;
    vel = velocidad;
    int     vel_izq = vel * VELOCIDAD_GIRO * Factor_vel;     //Correccion de la velocidad maxima mediante constantes de valor maximo 1
    float izq_ade = map(angulo, 120, 160, 0, 100);
    izq_ade = (100 - izq_ade) / 100;
    analogWrite (PWM_Derecha, vel_izq * izq_ade);             //Habilito las dos PWM's
    analogWrite (PWM_Izquierda, vel_izq );
    //Serial.print ("\n\n Motor IZQUIERDA. ");   //Imprimo por el puerto serie para saber en que estado estoy.
    delay(50);
  }

  else if ((angulo > 300) && (angulo < 340)) //ATRAS-DERECHA
  {
    if (sentido_derecha == 0)
    {
      sentido_derecha = 1;
      analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
      analogWrite (PWM_Izquierda, 0);
      sentido_adelante = 0; sentido_izquierda = 0; sentido_atras = 0;
    }
    digitalWrite (Dir_Derecha1, LOW);          //Pongo las direcciones a 01 ATRAS.
    digitalWrite (Dir_Derecha2, HIGH);
    digitalWrite (Dir_Izquierda1, LOW);        //Pongo las direcciones a 01 ATRAS.
    digitalWrite (Dir_Izquierda2, HIGH);

    digitalWrite (Enable_Derecha, true);       //Conecta los dos reles.
    digitalWrite (Enable_Izquierda, true);
    a1 = (vx - lim_sup);
    a2 = (1023 - lim_sup);
    velocidad = a1 * VEL / a2;
    vel = velocidad;
    int     vel_der = vel * VELOCIDAD_GIRO * Factor_vel;      //Correccion de la velocidad maxima mediante constantes de valor maximo 1
    float der_atr = map(angulo, 300, 340, 0, 100);
    der_atr = der_atr / 100;
    analogWrite (PWM_Derecha, vel_der * der_atr);        //Habilito las dos PWM's
    analogWrite (PWM_Izquierda, vel_der);
    //Serial.print ("\n\n Motor ATRAS-DERECHA. ");      //Imprimo por el puerto serie para saber en que estado estoy.
    delay(50);
  }
  else if ((angulo < 240) && (angulo > 200)) //ATRAS-IZQUIERDA
  {
    if (sentido_izquierda == 0)
    {
      sentido_izquierda = 1;
      analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
      analogWrite (PWM_Izquierda, 0);
      sentido_adelante = 0; sentido_derecha = 0; sentido_atras = 0;
    }
    analogWrite (PWM_Derecha, 0);              //Deshabilito las dos PWM's
    analogWrite (PWM_Izquierda, 0);
    digitalWrite (Dir_Derecha1, LOW);          //Pongo las direcciones a 01 ATRAS.
    digitalWrite (Dir_Derecha2, HIGH);
    digitalWrite (Dir_Izquierda1, LOW);        //Pongo las direcciones a 01 ATRAS.
    digitalWrite (Dir_Izquierda2, HIGH);

    digitalWrite (Enable_Derecha, true);       //Conecta los dos reles.
    digitalWrite (Enable_Izquierda, true);
    a1 = (lim_inf - vx);
    a2 = (lim_inf);
    velocidad = a1 * VEL / a2;
    vel = velocidad;
    int     vel_izq = vel * VELOCIDAD_GIRO * Factor_vel;     //Correccion de la velocidad maxima mediante constantes de valor maximo 1
    float izq_atr = map(angulo, 200, 240, 0, 100);
    izq_atr = (100 - izq_atr) / 100;
    analogWrite (PWM_Derecha, vel_izq);              //Habilito las dos PWM's
    analogWrite (PWM_Izquierda, vel_izq * izq_atr);
    //Serial.print ("\n\n Motor ATRAS-IZQUIERDA. ");   //Imprimo por el puerto serie para saber en que estado estoy.
    delay(50);
  }

  /******* CODIGO DE SELECCION DE VELOCIDAD ************/

  if  (accion_vel < 100) //Pulsado a nivel BAJO, VELOCIDAD--
  {
    nivel_velocidad = nivel_velocidad - 1;
    if (nivel_velocidad < 0)
    {
      nivel_velocidad = 0;
      Factor_vel = 0.75;
    }

    Factor_vel = Factor_vel - 0.1;

    int velocidad_disp = map(nivel_velocidad, 0, 5, 0, 200);
    //Serial.println (nivel_velocidad);
    analogWrite(Referencia_vel_out, velocidad_disp);
    //Serial.print ("\n\n Se ha pulsado el botón de VELOCIDAD--. ");   //Imprimo por el puerto serie para saber en que estado estoy.
    delay(200);
  }
  else if ((accion_vel > 300) && (accion_vel < 700)) //Pulsado a nivel MEDIO, VELOCIDAD++
  {
    nivel_velocidad = nivel_velocidad + 1;
    if (nivel_velocidad > 5)
    {
      nivel_velocidad = 5;
      Factor_vel = 0.75;
    }
    Factor_vel = Factor_vel + 0.1;

    velocidad_disp = map(nivel_velocidad, 0, 5, 10, 200);
    
  if (velocidad_disp <= 10)
  {velocidad_disp = 0;}
  if ((velocidad_disp > 10) && (velocidad_disp <= 50))
  {velocidad_disp = 50;}
  if ((velocidad_disp > 50) && (velocidad_disp <= 90))
  {velocidad_disp = 80;}
  if ((velocidad_disp > 90) && (velocidad_disp <= 130))
  {velocidad_disp = 120;}
  if ((velocidad_disp > 130) && (velocidad_disp <= 180))
  {velocidad_disp = 150;}
  if (velocidad_disp > 180)
  {velocidad_disp = 200;}
  
    //Serial.println (nivel_velocidad);
    analogWrite(Referencia_vel_out, velocidad_disp);
    //Serial.print ("\n\n Se ha pulsado el botón de VELOCIDAD++. ");   //Imprimo por el puerto serie para saber en que estado estoy.
    delay(200);
  }
}

