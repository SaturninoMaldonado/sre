 // This software is the low level control of LOLA the 
// robot of GRAM from Alcalá University.
// You can find more information in 

// http://agamenon.tsc.uah.es/Investigacion/gram/research.html
// Please include reference 
// ................................

// Date   23rd of May 2019

// Version for the SRE controlled by buttons
// This version just take the input from 4 buttons
// PIN_FORWARD  indicates move foward with the same speed for both wheels
// PIN_BACKWARD indicates move backward with the same speed for both wheels
// PIN_RIGHT    indicates turn right fixing the right wheel
// PIN_LEFT     indicates turn left fixing the left wheel

// For thw two first movements the encoder and a PID controller is used
// and for the last movements the information of encoder are not used.
#define VERSION "V1.10"

#define TIME_PID        200 // Time in miliseconds for each iteration of the PID
  
#define IGNORE_PULSE   3000 // time in micros to ignore encoder pulses if faster
// Auxiliar variables to filter false impulses from encoders
volatile unsigned long auxr=0,auxl=0;
// Auxiliar variables to keep micros() at encoders
unsigned long tr,tl;

// PID (pd) constants
  float kp =  0.21*1.5;
  float kd = 10.0*1.5;

unsigned char SPEED_INI_L=255;  // 170
unsigned char SPEED_INI_R=255;  // 100

// PINS SPECIFICATIONS
// connect motor controller pins to Arduino digital pins
  //R Motor
  #define MOT_R_PWM_PIN   10
  #define MOT_R_A_PIN     28
  #define MOT_R_B_PIN     24
//  #define MOT_R_ENC_A_PIN 21      Only one pin is used
  #define MOT_R_ENC_B_PIN 20
  //L Motor
  #define MOT_L_PWM_PIN   11
  #define MOT_L_A_PIN     26
  #define MOT_L_B_PIN     22
//  #define MOT_L_ENC_A_PIN 19      Only one pin is used
  #define MOT_L_ENC_B_PIN 18
  
// set buttons pin to inputs
  #define PIN_FORWARD     34
  #define PIN_BACKWARD   36
  #define PIN_LEFT       38
  #define PIN_RIGHT      40

  #define LED 13


// Direction of movement
unsigned char dir_right,dir_left;

// Variables to keep each encoder pulse
volatile unsigned int encoderIZQ = 0, encoderDER = 0;
// Variables to obtain robot position and orientation (X, Y Theta)
unsigned int aux_encoderIZQ = 0, aux_encoderDER = 0;
volatile signed int encoder = 0;


// Radios relation allows to describe a curcular movement
float Radios_relation=1.0;

// Indicate the PWM that is applied to the motors
int velr = SPEED_INI_R;
int vell = SPEED_INI_L;
int error = 0;
int encoder_ant;
unsigned char orden[1];

unsigned char flag_boton=0;

//////////////////////////////////////////////////
//  Right encoder interrupt using only one pin
//////////////////////////////////////////////////
void cuentaDER()
{
  tr=micros();
  // if pulse is too fast from previoius is ignored
  if (tr-auxr>IGNORE_PULSE)
  {
//    Serial.print(" Tiempo ");
//    Serial.println(tr-auxr);
    auxr=tr;
    encoderDER++;    //Add one pulse
  }
//  encoder++;    //Add one pulse
}  // end of cuentaDER

//////////////////////////////////////////////////
//  Left encoder interrupt using only one pin
//////////////////////////////////////////////////
void cuentaIZQ()
{
  tl=micros();
  // if pulse is too fast from previoius is ignored
  if (tl-auxl>IGNORE_PULSE)
  {
    auxl=tl;
    encoderIZQ++;  //Add one pulse
  }
//  encoder--;  //Add one pulse
}  // end of cuentaIZQ

//////////////////////////////////////////////////
//  SETUP
//////////////////////////////////////////////////
void setup()
{
  // Add the interrupt lines for encoders
  attachInterrupt(digitalPinToInterrupt(MOT_R_ENC_B_PIN), cuentaDER, FALLING);
  attachInterrupt(digitalPinToInterrupt(MOT_L_ENC_B_PIN), cuentaIZQ, FALLING);

  // set all the motor control pins to outputs
  pinMode(MOT_R_PWM_PIN, OUTPUT);
  pinMode(MOT_L_PWM_PIN, OUTPUT);
  
  pinMode(MOT_R_A_PIN, OUTPUT);
  pinMode(MOT_R_B_PIN, OUTPUT);
  pinMode(MOT_L_A_PIN, OUTPUT);
  pinMode(MOT_L_B_PIN, OUTPUT);

  digitalWrite(MOT_R_A_PIN, LOW);
  digitalWrite(MOT_R_B_PIN, LOW);    
  digitalWrite(MOT_L_A_PIN, LOW);
  digitalWrite(MOT_L_B_PIN, LOW);

  analogWrite(MOT_R_PWM_PIN, 0);
  analogWrite(MOT_L_PWM_PIN, 0);
  
  
  // set encoder pins to inputs
  pinMode(MOT_L_ENC_B_PIN, INPUT);
  pinMode(MOT_R_ENC_B_PIN, INPUT);
  
  // set buttons pins
  pinMode(PIN_FORWARD, INPUT_PULLUP);
  pinMode(PIN_BACKWARD, INPUT_PULLUP);
  pinMode(PIN_LEFT, INPUT_PULLUP);
  pinMode(PIN_RIGHT, INPUT_PULLUP);
  
  pinMode(LED, OUTPUT);

  Serial.begin(38400);      //init the serial port  
  Serial.print("LOLA INI ");
  Serial.println(VERSION);

}  // end of setup



//////////////////////////////////////////////////
//  MOVE MOTORS:
//  dir_right (1: foward / 0: backwards)
//  dir_left  (1: foward / 0: backwards)
// 
//
//////////////////////////////////////////////////
void move_motors()
{
  // now turn off motors
  // Adaptation for L298n
  encoderIZQ = 0;
  encoderDER = 0;
  aux_encoderIZQ = 0;
  aux_encoderDER = 0;
  encoder = 0;
  encoder_ant=0;

  if (dir_right==1)
  {
    // Right motor
    digitalWrite(MOT_R_A_PIN, LOW);
    digitalWrite(MOT_R_B_PIN, HIGH);
  }
  else
  {
    // Right motor
    digitalWrite(MOT_R_A_PIN, HIGH);
    digitalWrite(MOT_R_B_PIN, LOW);    
  }
  
  if (dir_left==1)
  {
    // Left motor
    digitalWrite(MOT_L_A_PIN, HIGH);
    digitalWrite(MOT_L_B_PIN, LOW);
  }
  else
  {
    // Left motor
    digitalWrite(MOT_L_A_PIN, LOW);
    digitalWrite(MOT_L_B_PIN, HIGH);   
  }
  velr=SPEED_INI_R;
  vell=SPEED_INI_L;

  // If any speed is zero breaking mode is activated for that wheel
  if (SPEED_INI_R==0)
  {
    digitalWrite(MOT_R_A_PIN, LOW);
    digitalWrite(MOT_R_B_PIN, LOW);        
    analogWrite(MOT_R_PWM_PIN, 255);
  }
  else
    analogWrite(MOT_R_PWM_PIN, velr);
    
  if (SPEED_INI_L==0)
  {
    digitalWrite(MOT_L_A_PIN, LOW);
    digitalWrite(MOT_L_B_PIN, LOW);        
    analogWrite(MOT_L_PWM_PIN, 255);
  }
  else
    analogWrite(MOT_L_PWM_PIN, vell);
  
}  // end move_motors



//////////////////////////////////////////////////
//  STOP_MOTORS
//////////////////////////////////////////////////
void stop_motors()
{
  
  // now turn off motors
  digitalWrite(MOT_R_A_PIN, LOW);
  digitalWrite(MOT_R_B_PIN, LOW);
  digitalWrite(MOT_L_A_PIN, LOW);
  digitalWrite(MOT_L_B_PIN, LOW);
  analogWrite(MOT_R_PWM_PIN, 255);
  analogWrite(MOT_L_PWM_PIN, 255);
}  // end stop_motors

//////////////////////////////////////////////////
//  SPEED_NORMALIZATION
//
//  Speeds are normalized in order to work 
//  at maximum speed give as SPEED_INI_X
//
//////////////////////////////////////////////////
void speed_normalization()
{
    if (velr>vell)
    {
      vell-=(velr-SPEED_INI_R);
      velr=SPEED_INI_R;
      if (vell<0)
        vell=0;          
    }
    else
    {
      velr-=(vell-SPEED_INI_L);
      vell=SPEED_INI_L;
      if (velr<0)
        velr=0;
    }
} // end of speed_normalization


//////////////////////////////////////////////////
//  STRAIGH_DIST
//////////////////////////////////////////////////
void straigh_dist()
{ 
  float s,sl,sr;
  long encoder_long;
  float aux_float;
  unsigned int temp_encDER;
  unsigned int temp_encIZQ;

  // To avoid overflow
  if (encoderDER>10000 && encoderIZQ>10000)
  {
    encoderDER-=1000;
    encoderIZQ-=1000;  
  }
  temp_encDER=encoderDER;
  temp_encIZQ=encoderIZQ;

  // encoder is the difference from both encoder
  // with the nornalization constant for wheel diameter error  
  aux_float=(float)temp_encIZQ-(float)temp_encDER;
  if (aux_float>0)
    aux_float+=0.499999;
  if (aux_float<0)
    aux_float-=0.499999;
  encoder=(int)aux_float;

  error = encoder_ant - encoder;
  encoder_ant = encoder;
  // Implement PID (just PD)
  // Right wheel speed is updated
  // If it is not breaking at the end of the movement
  aux_float=(float)encoder * kp - (float)error * kd;
  if (aux_float>0)
    aux_float+=0.5;
  if (aux_float<0)
    aux_float-=0.5;

  velr += (int)aux_float;
  
  speed_normalization();

  // Write in PWM the speeds for each wheel
  analogWrite(MOT_R_PWM_PIN, velr);
  analogWrite(MOT_L_PWM_PIN, vell);          
}  // fin de straigh_dist()





//////////////////////////////////////////////////
//  DEP:
// This function is used for depuration
// 
//////////////////////////////////////////////////
void dep()
{
  Serial.print(" VR: ");
  Serial.print(velr);
  Serial.print(" VL: ");
  Serial.print(vell);
  Serial.print(" encoderDER: ");
  Serial.print(encoderDER);     
  Serial.print(" encoderIZQ: ");
  Serial.println(encoderIZQ);     
}  // fin de dep()




//////////////////////////////////////////////////
//  INIT_MOV
//  unsigned char direc: Indicating move foward (1) or backward (0)
//  unsigned char sp_r:  Speed for the right motor
//  unsigned char sp_l:  Speed for the left motor
//////////////////////////////////////////////////
void init_mov(unsigned char direc,unsigned char sp_r,unsigned char sp_l)
{
  static unsigned long time=0;
  volatile unsigned char flag_reset=1;
  volatile static unsigned char dir_aux=255;
  volatile static unsigned char sp_r_aux=0;
  volatile static unsigned char sp_l_aux=0;

  dep();
  if (dir_aux==direc && sp_r_aux==sp_r && sp_l_aux==sp_l)
    flag_reset=0;
  else 
    flag_reset=1;

  dir_aux=direc;
  sp_r_aux=sp_r;
  sp_l_aux=sp_l;
  
      SPEED_INI_R=sp_r;
      SPEED_INI_L=sp_l;             
      dir_right=direc;
      dir_left=direc;
      if (flag_boton==0 || flag_reset)
      {
        stop_motors(); 
        move_motors();           
        flag_boton=1;
      }
      digitalWrite(LED,1);
      
      if(millis()-time>TIME_PID)
      {
          time=millis();
          if (SPEED_INI_R!=0 && SPEED_INI_L!=0)
            straigh_dist();
          dep();
      }
}  // fin de void iniciar_mov(unsigned char direccion)

//////////////////////////////////////////////////
//  LOOP
//////////////////////////////////////////////////
void loop()
{
  static unsigned long time1=0;

  while(1)
  {
  
    if (digitalRead(PIN_FORWARD)==0)
    {
      init_mov(1,255,255);
    }     
    else
    {
      if (digitalRead(PIN_BACKWARD)==0)
      {
        init_mov(0,255,255);
      }
      else
        if (digitalRead(PIN_LEFT)==0)
        {
          init_mov(1,255,0);
        }
        else
          if (digitalRead(PIN_RIGHT)==0)
          {
            init_mov(1,0,255);
          }
          else
          {
            // Just in case an error produce movement of the motors
            stop_motors(); 
            flag_boton=0; 
            digitalWrite(LED,0);

            if((millis()-time1)>5000)
            {
              Serial.print(".");
              time1=millis();
            }
          }
        }
 }// end of while(1)
}  // end of loop
