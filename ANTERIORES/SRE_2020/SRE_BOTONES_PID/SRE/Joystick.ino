#include <math.h>

//Indicate the analog inputs 
const int X_Pin = A12;
const int Y_Pin = A11;

//const float pi = 3.1416;

//buena//int cizq[]={75,76,78,79,81,82,84,86,87,89,90,92,94,95,97,98,95,88,82,75,68,62,55,48,42,35,28,22,15,8,2,-5,-71,-72,-73,-74,-75,-76,-77,-78,-79,-80,-80,-81,-82,-83,-84,-85,-83,-76,-70,-64,-57,-51,-45,-38,-32,-26,-19,-13,-7,0,6,12};
//buena//int cdch[]={-7,-1,6,13,20,27,34,41,48,55,62,69,76,83,90,97,96,95,93,91,90,88,87,85,83,82,80,79,77,75,74,72,7,1,-6,-12,-18,-24,-30,-36,-42,-48,-54,-60,-66,-72,-78,-84,-84,-83,-81,-80,-78,-76,-75,-73,-72,-70,-68,-67,-65,-64,-62,-61};

int cizq[]={88,88,89,89,90,91,91,92,93,93,94,94,95,96,96,97,95,89,84,78,72,67,61,55,35,22,9,-3,-16,-29,-41,-54,-71,-72,-73,-74,-75,-76,-77,-78,-79,-80,-80,-81,-82,-83,-84,-85,-83,-76,-70,-64,-57,-51,-45,-38,-30,-17,-4,8,21,34,47,59};
int cdch[]={-40,-29,-18,-6,5,17,28,39,48,55,62,69,76,83,90,97,96,96,95,94,94,93,93,92,91,91,90,89,89,88,87,87,63,51,38,25,13,0,-13,-26,-42,-48,-54,-60,-66,-72,-78,-84,-84,-83,-81,-80,-78,-76,-75,-73,-72,-70,-68,-67,-65,-64,-62,-61};

int X_Value = 0;
int Y_Value = 0;

float modulo=0;
double angle=0;
int indice=0;

unsigned short int velr_aux=0;
unsigned short int vell_aux=0;

void read_joystick(){

  X_Value=analogRead(X_Pin)-510;
  Y_Value=analogRead(Y_Pin)-510;

  if ((X_Value < 30)&& (X_Value>-30)){
    X_Value=0;
  }
  if ((Y_Value < 30)&& (Y_Value>-30)){
    Y_Value=0;
  }

  if (X_Value>114)
    X_Value=114;
  
  if (X_Value<-114)
    X_Value=-114;
  
  if (Y_Value>114)
    Y_Value=114;  
  
  if (Y_Value<-114)
    Y_Value=-114;
      
  modulo=sqrt(pow(X_Value,2)+pow(Y_Value,2));
  angle=atan2(Y_Value,X_Value);

  if (angle<0)
      angle=angle+2*M_PI;

  indice=floor(64*angle/(2*M_PI));
  
 vell_aux = abs(255.00/95.00*cizq[indice]*modulo/114.00);
 velr_aux = abs(255.00/96.00*cdch[indice]*modulo/114.00);

 if (vell_aux>255)
 {
  vell_aux=255;
 }
 if (velr_aux>255)
 {
  velr_aux=255;
 }

  SPEED_INI_L=vell_aux;
  SPEED_INI_R=velr_aux;
  
}

void dep_Joystick(){

   if(digitalRead(SW5_PIN)==LOW){
    
    Serial2.print("X_VALUE: ");
    Serial2.print(X_Value);
    Serial2.print("  Y_VALUE: ");
    Serial2.print(Y_Value);
    Serial2.print(" Motor_izquierdo: ");
    Serial2.print(SPEED_INI_L);
    Serial2.print("  Motor_derecho ");
    Serial2.println(SPEED_INI_R);
    Serial2.print("  indice: ");
    Serial2.print(indice);
    Serial2.print("  modulo: ");
    Serial2.print(modulo);
    Serial2.print("  angulo: ");
    Serial2.println(angle);
    
   }
   else {
    
    Serial.print("X_VALUE: ");
    Serial.print(X_Value);
    Serial.print("  Y_VALUE: ");
    Serial.print(Y_Value);
    Serial.print(" Motor_izquierdo: ");
    Serial.print(SPEED_INI_L);
    Serial.print("  Motor_derecho ");
    Serial.println(SPEED_INI_R);
    Serial.print("  indice: ");
    Serial.print(indice);
    Serial.print("  modulo: ");
    Serial.print(modulo);
    Serial.print("  angulo: ");
    Serial.println(angle);
    Serial.print("  CIZQ: ");
    Serial.println(cizq[indice]);
    Serial.print("  CDER: ");
    Serial.println(cdch[indice]);    
   }

   delay(10);
  
}

void SRE_Joystick(){

  while(1){

    if(digitalRead(SW1_PIN)==HIGH)
    {
          if(digitalRead(SW5_PIN)==LOW)
          Serial2.println("Seleccione mediante el SWITCH de la placa un modo de funcionamiento");
          else
          Serial.println("Seleccione mediante el SWITCH de la placa un modo de funcionamiento");
          break;
    }
    read_joystick();

    if(cizq[indice]>0)
      dir_left=1;
    else 
      dir_left=0; 

    if(cdch[indice]>0)
      dir_right=1;
    else 
      dir_right=0;

    move_motors();


    dep_Joystick();
   

    delay(10); 

  }


}
