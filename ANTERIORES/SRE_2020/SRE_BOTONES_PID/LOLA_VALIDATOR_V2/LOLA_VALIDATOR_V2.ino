#include "lola_board.h"

unsigned long l_pulses = 0;
unsigned long r_pulses = 0;

void setup() {
  Serial.begin(9600);
  
  hw_init();

  //begin testing
  print_message();

  //Init the ecoders:
  
  //Left motor
  attachInterrupt(digitalPinToInterrupt(3), enc_L_A, RISING); //A channel
  //attachInterrupt(digitalPinToInterrupt(52), enc_L_B, RISING); //B channel
  //Right
  attachInterrupt(digitalPinToInterrupt(2), enc_R_A, RISING); //A channel
  //attachInterrupt(digitalPinToInterrupt(50), enc_R_B, RISING); //B channel
}

void loop() {
 lola_validate();  
}//END LOOP


void hw_init(){
//Battery pin for voltaje measurement
  pinMode(BAT_PIN,         INPUT);

//Dip switch for configuration
  pinMode(SW1_PIN,  INPUT_PULLUP);
  pinMode(SW2_PIN,  INPUT_PULLUP);
  pinMode(SW3_PIN,  INPUT_PULLUP);
  pinMode(SW4_PIN,  INPUT_PULLUP);
  pinMode(SW5_PIN,  INPUT_PULLUP);
  pinMode(SW6_PIN,  INPUT_PULLUP);
  pinMode(SW7_PIN,  INPUT_PULLUP);
  pinMode(SW8_PIN,  INPUT_PULLUP);

//Buzzer
  pinMode(BUZZER_PIN, OUTPUT);

//L Motor
  pinMode(MOT_L_PWM_PIN,  OUTPUT);
  pinMode(MOT_L_A_PIN,    OUTPUT);
  pinMode(MOT_L_B_PIN,    OUTPUT);
  pinMode(MOT_L_ENC_A_PIN, INPUT);
  pinMode(MOT_L_ENC_B_PIN, INPUT);

//R Motor
  pinMode(MOT_R_PWM_PIN,  OUTPUT);
  pinMode(MOT_R_A_PIN,    OUTPUT);
  pinMode(MOT_R_B_PIN,    OUTPUT);
  pinMode(MOT_R_ENC_A_PIN, INPUT);
  pinMode(MOT_R_ENC_B_PIN, INPUT);

//L RGB LED
  pinMode(L_RED_PIN,      OUTPUT);
  pinMode(L_GRE_PIN,      OUTPUT);
  pinMode(L_BLU_PIN,      OUTPUT);
  
//R RGB LED
  pinMode(R_RED_PIN,      OUTPUT);
  pinMode(R_GRE_PIN,      OUTPUT);
  pinMode(R_BLU_PIN,      OUTPUT);

//F Ultrasound sensor
  pinMode(F_US_TRIG,      OUTPUT);
  pinMode(F_US_ECHO,      INPUT);
//L Ultrasound sensor
  pinMode(L_US_TRIG,      OUTPUT);
  pinMode(L_US_ECHO,      INPUT);
//R Ultrasound sensor
  pinMode(R_US_TRIG,      OUTPUT);
  pinMode(R_US_ECHO,      INPUT);
//B Ultrasound sensor
  pinMode(R_US_TRIG,      OUTPUT);
  pinMode(R_US_ECHO,      INPUT);

//Botonera

  pinMode(AUX1_2,  INPUT_PULLUP);
  pinMode(AUX2_1,  INPUT_PULLUP);
  pinMode(AUX2_2,  INPUT_PULLUP);
  pinMode(AUX3_1,  INPUT_PULLUP);
}

void enc_L_A (){
  l_pulses++;
}

void enc_R_A (){
  r_pulses++;
}
