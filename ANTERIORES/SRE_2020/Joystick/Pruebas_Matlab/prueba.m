%             izquierda     derecha
%   90º          100          100
%   45º           75            0
%    0º           50          -50
%  -45º            0          -55
%  -90º          -60          -60
% -135º          -55            0
% -180º          -50           50
%  135º            0           75

%Llamada a la función que rellena los vectores cizq y cdch
%datos;
cdch=floor(cdch);
cizq=floor(cizq);

x=114;
y=114;
fig=figure(1);
subplot(221);
 plot(x,y,'or','LineWidth',5);
    title('Posición de Joystick');
    axis([0 228 0 228]);
    grid on
    subplot(212);
    bar([0 0]);
    title('Módulo y Fase del Joy');
    axis([1 2 -1 1])

while(waitforbuttonpress)
    a=fig.CurrentCharacter;
    
    if (a=='s')
        x=x+1;
        if (x>228)
            x=228;
            beep;
        end        
    end
    if (a=='a')
        x=x-1;
        if (x<0)
            x=0;
            beep;
        end        
    end
    if (a=='o')
        y=y+1;
        if (y>228)
            y=228;
            beep;
        end        
    end
    if (a=='k')
        y=y-1;
        if (y<0)
            y=0;
            beep;
        end        
    end

    subplot(221);
    plot(x,y,'or','LineWidth',5);
    title('Posición de Joystick');
    axis([0 228 0 228]);
    grid on
    X=x-114;
    Y=y-114;
    
    modulo=sqrt(X^2+Y^2);
    angle=atan2(Y,X);
    subplot(212);
    bar([modulo/114 angle/(pi)]);
    title('Módulo y Fase del Joy');
    axis([1 2 -1.5 1.5])
    if (angle<0)
        angle=angle+2*pi;
    end
    indice=1+floor(64*angle/(2*pi));
    Motor_izquierdo=floor(255/95*cizq(indice)*modulo/114);
    Motor_derecho  =floor(255/96*cdch(indice)*modulo/114);
    subplot(222);
    bar([Motor_izquierdo Motor_derecho]);
    title('Motores');
    axis([1 2 -255 255])
    [Motor_izquierdo Motor_derecho modulo angle cizq(indice) cdch(indice) indice X Y]
end