I=[ 50 75 100  0 -50 -55 -60   0 50];
D=[-50  0 100 75  50   0 -60 -55 -50]; 
figure(2);
plot(0:8,I,'-o');
hold on
plot(0:8,D,'-*');
hold off
legend(['Izq';'Der']);
axis([0 8 -100 100])

% p_i = polyfit([0:2:4]/8,I(1:2:5),2);
% nn=0:0.1:4;
% nn=nn/8;
% ci=p_i(3)+ p_i(2)*nn   +p_i(1)*nn.^2;y
% %ci=p_i(4)+ p_i(3)*nn   +p_i(3)*nn.^2+p_i(2)*nn.^3+p_i(1)*nn.^4;
% hold on
% plot(nn*8,ci);
% hold off


n=0:0.0997:2*pi;
p1= 2*10/pi;  %50
p2=-2*90/pi; %150
p22=-2*200/pi;
p3=-2*15/pi;  %10
p4= 2*100/pi; %110
p41= 2*200/pi;

q1= 2*110/pi; %150
q11=2*180/pi;
q2=-2*10/pi;  %50
q3=-2*200/pi; %110
q31=-2*95/pi;
q4= 2*25/pi;  %10
cizq=zeros(1,length(n));
cdch=zeros(1,length(n));
for c=1:length(n)
  if (n(c)>=0 & n(c)<pi/4)
      cizq(c)=n(c)*p1+88;
      cdch(c)=n(c)*q11-40;
  end
  if (n(c)>=pi/4 & n(c)<pi/2)
      cizq(c)=n(c)*p1+88;
      cdch(c)=n(c)*q1-7;
  end
  if (n(c)>=pi/2 & n(c)<3*pi/4)
      cizq(c)=(n(c)-pi/2)*p2+97;
      cdch(c)=(n(c)-pi/2)*q2+97;
  end
  if (n(c)>=3*pi/4 & n(c)<pi)
      cizq(c)=(n(c)-pi/2)*p22+140;
      cdch(c)=(n(c)-pi/2)*q2+97;
  end
  if (n(c)>=pi & n(c)<5*pi/4)
      cizq(c)=(n(c)-pi)*p3-70;
      cdch(c)=(n(c)-pi)*q3+70;
  end
  if (n(c)>=5*pi/4 & n(c)<3*pi/2)
      cizq(c)=(n(c)-pi)*p3-70;
      cdch(c)=(n(c)-pi)*q31+10;
  end
  if (n(c)>=3*pi/2 & n(c)<7*pi/4)
      cizq(c)=(n(c)-3*pi/2)*p4-87;
      cdch(c)=(n(c)-3*pi/2)*q4-85;
  end
  if (n(c)>=7*pi/4 )
      cizq(c)=(n(c)-3*pi/2)*p41-140;
      cdch(c)=(n(c)-3*pi/2)*q4-85;
  end
end
% for c=1:length(n)
%   if (n(c)>=0 & n(c)<pi/2)
%       cizq(c)=n(c)*p1+50;
%       cdch(c)=n(c)*q1-50;
%   end
%   if (n(c)>=pi/2 & n(c)<pi)
%       cizq(c)=(n(c)-pi/2)*p2+100;
%       cdch(c)=(n(c)-pi/2)*q2+100;
%   end
%   if (n(c)>=pi & n(c)<3*pi/2)
%       cizq(c)=(n(c)-pi)*p3-50;
%       cdch(c)=(n(c)-pi)*q3+50;
%   end
%   if (n(c)>=3*pi/2 )
%       cizq(c)=(n(c)-3*pi/2)*p4-60;
%       cdch(c)=(n(c)-3*pi/2)*q4-60;
%   end
% end
    
hold on
plot(4*n/pi,cizq,'*--','LineWidth',2)
plot(4*n/pi,cdch,'o--','LineWidth',2)
plot([0 8],[0 0],'k');
%pause;
hold off






